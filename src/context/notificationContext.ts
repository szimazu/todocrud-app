import React, { createContext } from "react";

interface NotificationContextProps {
  submitted: boolean;
}

interface Action {
  type: "SHOW_SUBMITTED" | "HIDE_SUBMITTED";
}

interface IContextProps {
  state: NotificationContextProps;
  dispatch: React.Dispatch<Action>;
}

export const initState = {
  submitted: false,
};

export const NotificationContext = createContext<IContextProps>({
  state: initState,
  dispatch: () => undefined,
});

export const notificationReducer = (
  state: NotificationContextProps,
  action: Action
) => {
  switch (action.type) {
    case "SHOW_SUBMITTED":
      return {
        ...state,
        submitted: true,
      };
    case "HIDE_SUBMITTED":
      return {
        ...state,
        submitted: false,
      };
    default:
      return state;
  }
};
