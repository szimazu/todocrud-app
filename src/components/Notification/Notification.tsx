import React, { useContext } from "react";

import Toast from "react-bootstrap/Toast";

import { NotificationContext } from "../../context/notificationContext";

import "./Notification.css";

export const Notification = () => {
  const { state, dispatch } = useContext(NotificationContext);

  return (
    <Toast
      onClose={() => dispatch({ type: "HIDE_SUBMITTED" })}
      show={state.submitted}
      delay={3000}
      autohide
      bg={"success"}
      className="submit-notification"
    >
      <Toast.Body>Submitted.</Toast.Body>
    </Toast>
  );
};
