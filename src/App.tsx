import React, { useState, useEffect, useReducer } from "react";

import {
  NotificationContext,
  notificationReducer,
  initState,
} from "./context/notificationContext";

import { TasksList, InputForm, TaskItemType } from "@skovoroda/todocrud-lib";
import { Notification } from "./components/Notification/Notification";

import filter from "lodash/filter";
import map from "lodash/map";

import { v4 as uuidv4 } from "uuid";

import "./App.css";

function App() {
  const [tasks, setTasks] = useState<TaskItemType[]>(
    JSON.parse(localStorage.getItem("tasks") || "[]")
  );
  const [lang, setLang] = useState("en");

  const [state, dispatch] = useReducer(notificationReducer, initState);

  const defaultValues: TaskItemType = {
    task: "",
    rate: "",
    phone: "",
    date: "2021-09-24",
  };

  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);

  const addTask = (task: TaskItemType) => {
    setTasks((prevTasks) => [
      ...prevTasks,
      { ...task, id: uuidv4(), done: false },
    ]);
    setTasks((prevTasks) => [
      ...prevTasks.sort((a, b) => (a.rate <= b.rate ? 1 : -1)),
    ]);
    dispatch({ type: "SHOW_SUBMITTED" });
  };

  const completeTask = (id: string) => {
    setTasks(
      map(tasks, (task: TaskItemType) =>
        task.id === id ? { ...task, done: true } : task
      )
    );
  };

  const deleteTask = (id: string) => {
    setTasks(filter(tasks, (task: TaskItemType) => id !== task.id));
  };

  return (
    <NotificationContext.Provider value={{ state, dispatch }}>
      <Notification />
      <div className="todocrud-app">
        <select
          className="lang-select"
          value={lang}
          onChange={(e) => setLang(e.currentTarget.value)}
        >
          <option value="en">English</option>
          <option value="de">German</option>
        </select>
        <div className="todocrud-app--input">
          <InputForm
            addTask={addTask}
            defaultValues={defaultValues}
            lang={lang}
          />
        </div>
        <div className="todocrud-app--tasks-list">
          <TasksList
            todoItems={tasks}
            completeTask={completeTask}
            deteteTask={deleteTask}
            lang={lang}
          />
        </div>
      </div>
    </NotificationContext.Provider>
  );
}

export default App;
